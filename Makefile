ifeq ($(OS),Windows_NT)
	PLTFRM = network/network_win.c platform/platform_win.c
	COMPONENTS = -lws2_32
else
	PLTFRM = network/network_lin.c platform/platform_lin.c
	COMPONENTS = -pthread 
endif

all: server client
	start server
	start client
	start client
	
client: $(PLTFRM) client.c
	gcc client.c $(PLTFRM) -o client.exe $(COMPONENTS)

server: $(PLTFRM) server.c
	gcc server.c $(PLTFRM) -o server.exe $(COMPONENTS)

	
