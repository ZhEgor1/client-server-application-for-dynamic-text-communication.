#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "network/network.h"
#include "platform/platform.h"

int connection;
char username[20];
char serverKey[32] = "ttMcZBlXR02lf9qBNapzNbAxgOMYvQ5";
int currentPort = 7300;
struct Handshake clientHS;

void* autoPortChange(void *args)
{	
	srand(time(0));
	while (1)
	{
		Delay(45000);
		int newPort = 49152 + rand() % (65535 - 49152);
		int notifications = -1;
		if (connection == -1) return 0;
		enum Packet packettype = P_NewPortRequest;
		Send(connection, (char*)&packettype, sizeof(enum Packet), 0);
		Send(connection, (char*)&newPort, sizeof(int), 0);
		Send(connection, (char*)&notifications, sizeof(int), 0);
	}
}
void disconnectServer(int notifications)
{
	Closesocket(connection);
	connection = -1;
	if (notifications == 1)
	{
		printf("Server disconnected\n");
	}
}
void startHandshake()
{
	enum Packet packettype = P_Handshake;
	Send(connection, (char*)&packettype, sizeof(enum Packet), 0);
}
int processPacket(enum Packet packettype)
{
	char msg[3500];
	int newPort;
	switch (packettype)
	{
		char protocolVersion[20];
	case P_ChatMessage:
		if (Recv(connection, msg, sizeof(msg), 0) == -1) return -1;
		printf("%s", msg);
		break;
	case P_Disconnect:
		return -1;
		break;
	case P_Handshake:
	
		Send(connection, (char*)&clientHS, sizeof(struct Handshake), 0);
		struct Handshake serverHS;
		if (Recv(connection, (char*)&serverHS, sizeof(struct Handshake), 0) != -1)
		{
			if(strcmp(serverKey, serverHS.key) != 0) 
			{
				printf("Server key didn't recognize\n");
			}
			else if( clientHS.protocolVersion > serverHS.protocolVersion)
			{
				printf("Protocol version not supported\n");
			}
			else
			{
				Send(connection, username, sizeof(username), 0);
				return 0;
			} 
		}
		printf("Handshake unsuccess\n");
		return -1;
		break;
	case P_NewPortRequest:
		Recv(connection, (char*)&newPort, sizeof(int), 0);
		int notifications;
		Recv(connection, (char*)&notifications, sizeof(int), 0);
	
		disconnectServer(notifications);
		struct sockaddr_in addr;
		BindAddr((struct sockaddr_in*)&addr, "127.0.0.1", newPort, AF_INET);
		connection = Socket(AF_INET, SOCK_STREAM, 0);
		int res = Connect(connection, (struct sockaddr*)&addr, sizeof(addr)); //If the port isn't working, we open the port by default
		if (res != -1)
		{
			currentPort = newPort;
			startHandshake();
			if (notifications == 1)
			{
				printf("Connected!\n");
			}
		}		
		break;
	default:
		printf("%s%d%c", "Unrecognized packet: ", packettype, '\n');
	}

	return 0;
}
void* clientHandler(void *args)
{
	enum Packet packettype;
	while(1)
	{
		if(Recv(connection, (char*)&packettype, sizeof(enum Packet), 0) == -1) break;
		if(processPacket(packettype) == -1) break;
	}
	disconnectServer(1);
}
void commandHandler(char* command)
{
	if(strstr(command, "/disconnect") != 0)
	{
		enum Packet packettype = P_Disconnect; 
		Send(connection, (char*)&packettype, sizeof(enum Packet), 0);
		return;
	}
	else if(strstr(command, "/newport") != 0)
	{
		int newPort = 0;

		if (command[8] != '<')
		{
			printf("Undefined command\n");
			return;	
		}
		int i = 9;
		while ((int)command[i] >= 48 && (int)command[i] <= 57)
		{
			newPort += (int)command[i] - 48;
			newPort *= 10; 
			i++;
		}
		newPort /= 10;
		if (command[i] != '>' || i == 9)
		{
			printf("Undefined command\n");
			return;	
		}
		if (newPort > 65535 || newPort < 0)
		{
			printf("Port outside the border\n");
			return;
		}
		else if (newPort == currentPort)
		{
			printf("%s%d%c", "You are already using this port: ", currentPort, '\n');
			return;
		}
		
		int notifications = 1;
		enum Packet packettype = P_NewPortRequest;
		Send(connection, (char*)&packettype, sizeof(enum Packet), 0);
		Send(connection, (char*)&newPort, sizeof(int), 0);
		Send(connection, (char*)&notifications, sizeof(int), 0);
	}
	else if(strstr(command, "/add") != 0) 
	{
		char friendsName[20];
		if (command[4] != '<')
		{
			printf("Undefined command\n");
			return;	
		}
		int i = 5;
		int j = 0;
		while (command[i] != '>')
		{
			if (j == 20)
			{
				printf("Undefined command\n");
				return;	
			}
			friendsName[j] = command[i];
			i++; j++;
		}
		if (j == 0)
		{
			printf("Undefined command\n");
			return;	
		}
		
		enum Packet packettype = P_ConnectWithUser;
		Send(connection, (char*)&packettype, sizeof(enum Packet), 0);
		Send(connection, friendsName, sizeof(friendsName), 0);
	}
	else
	{
		printf("Undefined command\nAll commands:\nAdd friend - \"\\add<username>\"\nDisconnect request - \"\\disconnect\"\nChoose another port - \"\\newport<PortNumber>\"\n");
	}
}
int creatMessage(char* msg)
{
	char textMsg[3440];
	fgets(textMsg, 3440, stdin);
	if (textMsg[0] == '/')
	{
		commandHandler(textMsg);
		return -1;
	}
	else if (textMsg[0] == '\n') return -1;
	else if (connection == -1) return -1;
	
	long int ttime = time(0);
	char messageSendingTime[25];
	strcpy(msg, "");
	strcpy(messageSendingTime, ctime(&ttime));
	messageSendingTime[24] = '\0';
	strcat(msg, username);
	strcat(msg, " [");
	strcat(msg, messageSendingTime);
	strcat(msg, "]: ");
	strcat(msg, textMsg);
}
void inputUsername()
{
	printf("Input username (no more than 20 characters): ");
	scanf("%20s", username);
}



int main(int argc, char** arg)
{
	strcpy(clientHS.key, "sCpts1DNk8qR0V3HYJAjqYpRgBOTACM");
	clientHS.protocolVersion = 1.0f;

	Init();
	struct sockaddr_in addr;
	BindAddr((struct sockaddr_in*)&addr, "127.0.0.1", DEFAULT_PORT, AF_INET);
	int defaultSListen = Socket(AF_INET, SOCK_STREAM, 0); 
	connection = defaultSListen;
	inputUsername();
	Connect(connection, (struct sockaddr*)&addr, sizeof(addr));
	startHandshake();
	
	ThreadCreate(autoPortChange, 0);
	ThreadCreate(clientHandler, 0);

	enum Packet packettype;
	char msg[3500];
	while(1)
	{
		if (creatMessage(msg) == -1) continue;	
		packettype = P_ChatMessage; 
		Send(connection, (char*)&packettype, sizeof(enum Packet), 0);
		Send(connection, msg, sizeof(msg), 0);
		Delay(50);
	}


	system("pause");
	return 0;
}