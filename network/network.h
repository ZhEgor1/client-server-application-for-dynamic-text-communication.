#include <stdio.h>
#include <stdlib.h>
#ifdef WIN32
    #include <winsock.h>
    #pragma comment(lib, "ws2_32.lib")
#elif __linux__
    #include <sys/socket.h>
    #include <sys/types.h>
    #include <netinet/in.h>
    #include <arpa/inet.h>
    #include <unistd.h>
#endif

int Accept(int sockfd, struct sockaddr *addr, int *addrlen);
int Bind(int sockfd, const struct sockaddr *addr, int addrlen);
void BindAddr(struct sockaddr_in *addr, const char * ip, unsigned short hostshort, short family);
int Closesocket(int sockfd);
int Connect(int sockfd, const struct sockaddr *addr, int addrlen);
int Getsockname(int sockfd, struct sockaddr *addr, int *addrlen);
void Init();
int Listen (int sockfd, int backlog);
int Recv(int sockfd, char* buf, int len, int flags);
int Send(int sockfd, const char* buf, int len, int flags);
int Setsockopt(int sockfd, int level, int optname, const void *optval, int optlen);
int Socket(int domain, int type, int protocol);