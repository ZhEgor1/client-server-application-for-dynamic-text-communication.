#include "network.h"

int Accept(int sockfd, struct sockaddr *addr, int *addrlen)
{
	int res = accept(sockfd, addr, addrlen);
	if (res  == -1)
	{
		printf("Accept failed\n");
	}
	return res;
}
int Bind(int sockfd, const struct sockaddr *addr, int addrlen)
{
	int res = bind(sockfd, addr, addrlen);
	if (res == -1)
	{
		printf("Bind failed\n");
	}
	return res;
}
void BindAddr(struct sockaddr_in *addr, const char* ip, unsigned short hostshort, short family)
{
	addr->sin_addr.s_addr = inet_addr(ip);
	addr->sin_port = htons(hostshort);
	addr->sin_family = family;
}
int Closesocket(int sockfd)
{
	int res = closesocket(sockfd);
	if (res == -1)
	{
		printf("Close socket failed\n");
	}
	return res;
}
int Connect(int sockfd, const struct sockaddr *addr, int addrlen)
{
	int res = connect(sockfd, addr, addrlen);
	if (res == -1)
	{
		printf("Connect failed\n");
	}
	return res;
}
int Getsockname(int sockfd, struct sockaddr *addr, int *addrlen)
{
	int res = Getsockname(sockfd, addr, addrlen);
	if (res == -1)
	{
		printf("Get socket name failed\n");
	}
	return res;
}
void Init()
{
	struct WSAData wsaData;
    WORD DLLVersion = MAKEWORD(2, 1);
    if (WSAStartup(DLLVersion, &wsaData) == -1)
    {
        printf("Error\n");
    }
}
int Listen (int sockfd, int backlog)
{
	int res = listen(sockfd, backlog);
	if (res == -1)
	{
		printf("Listen failed\n");
	}
	return res;
}
int Recv(int sockfd, char* buf, int len, int flags)
{
	int res = recv(sockfd, buf, len, flags);
	if (res == -1)
	{
		printf("Receive failed\n");
	}
	return res;
}
int Send(int sockfd, const char* buf, int len, int flags)
{	
	int res = send(sockfd, buf, len, flags);
	if (res == -1)
	{
		printf("Send failed\n");
	}
	return res;
}
int Setsockopt(int sockfd, int level, int optname, const void *optval, int optlen)
{
	int res = setsockopt(sockfd, level, optname, optval, optlen);
	if (res == -1)
	{
		printf("Setsockopt failed\n");
	}
	return res;
}
int Socket(int domain, int type, int protocol)
{
	int res = socket(domain, type, protocol);
	if (res == -1)
	{
		printf("Socket failed\n");
	}
	return res;
}