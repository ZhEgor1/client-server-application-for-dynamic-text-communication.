#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "network/network.h"
#include "platform/platform.h"

const int maxUsers = 100;
int hashSocketsCreatedCheck[65535];
unsigned int counterOfUsers = 0;
char clientKey[32] = "sCpts1DNk8qR0V3HYJAjqYpRgBOTACM";
struct Handshake serverHS;
struct User users[100];

void disconnectClient(int index)
{
	struct sockaddr_in addr;
	int sizeofaddr = sizeof(addr);
	// Getsockname(users[index].connection, (struct sockaddr*)&addr, &sizeofaddr);
	// if (addr.sin_port == htons(DEFAULT_PORT)) // we can't close default port
	// {
	// 	printf("We can't close default port\n");
	// 	return;
	// }
	Closesocket(users[index].connection);
	users[index].connection = -1;
	printf("%s%d%c", "Client disconnected: #", index, '\n');
}
int processPacket(int index, enum Packet packettype)
{
	char msg[3500];
	int newPort;
	char friendsName[20];
	char answer[70];

	switch (packettype)
	{
	case P_ChatMessage:
		if (Recv(users[index].connection, msg, sizeof(msg), 0) == -1) return -1;
		if (users[index].id_companion != index && users[index].id_companion != -1 && users[index].connection != -1)
		{
			Send(users[users[index].id_companion].connection, (char*)&packettype, sizeof(enum Packet), 0);
			Send(users[users[index].id_companion].connection, msg, sizeof(msg), 0);
		}
		break;
	case P_Disconnect:
		Send(users[index].connection, (char*)&packettype, sizeof(enum Packet), 0);
		users[users[index].id_companion].id_companion = -1;
		return -1;
	break;
	case P_Handshake:
		printf("%s%d%c", "Handshaking: #", index, '\n');
		struct Handshake clientHS;

		Send(users[index].connection, (char*)&packettype, sizeof(enum Packet), 0);
		if (Recv(users[index].connection, (char*)&clientHS, sizeof(struct Handshake), 0) != -1)
		{
			if(strcmp(clientKey, clientHS.key) != 0) 
			{
				printf("Client key didn't recognize\n"); 
			}
			else if( clientHS.protocolVersion > serverHS.protocolVersion)
			{
				printf("Protocol version not supported\n");
			}
			else
			{
				Send(users[index].connection, (char*)&serverHS, sizeof(struct Handshake), 0);
				Recv(users[index].connection, users[index].username, sizeof(users[index].username), 0);
				printf("%s%d%s","Handshake success: #", index, "\n\n");
				return 0;
			}
		}
		printf("%s%d%c","Handshake unsuccess: #", index, '\n');
		return -1;
	break;
	case P_NewPortRequest:
		Recv(users[index].connection, (char*)&newPort, sizeof(int), 0);
		int notifications;
		Recv(users[index].connection, (char*)&notifications, sizeof(int), 0);
		if (hashSocketsCreatedCheck[newPort] == 1)
		{
			printf("%s%d%c", "Request port failed: ", index, '\n');
			if (notifications == 1)
			{
				strcpy(answer, "Request port failed\n");
				packettype = P_ChatMessage;
				Send(users[index].connection, (char*)&packettype, sizeof(enum Packet), 0);
				Send(users[index].connection, (char*)&answer, sizeof(answer), 0);
			}
			return 0;
		}
		enum Packet packettype = P_NewPortRequest;
		Send(users[index].connection, (char*)&packettype, sizeof(enum Packet), 0);
		Send(users[index].connection, (char*)&newPort, sizeof(int), 0);
		disconnectClient(index);

		struct sockaddr_in addr;
    	int sizeofaddr = sizeof(addr);
		BindAddr((struct sockaddr_in*)&addr, "127.0.0.1", newPort, AF_INET);
		int sListen = Socket(AF_INET, SOCK_STREAM, 0);
		Bind(sListen, (struct sockaddr*)&addr, sizeof(addr));
    	Listen(sListen, 1);
	    users[index].connection = Accept(sListen, (struct sockaddr*)&addr, &sizeofaddr);
		hashSocketsCreatedCheck[newPort] = 1;

		printf("%s%d%s%d%s", "Client connected: #", index, " - port<", newPort, ">\n");
		if (notifications == 1)
		{
			strcpy(answer, "Request port successful\n");
			packettype = P_ChatMessage;
			Send(users[index].connection, (char*)&packettype, sizeof(enum Packet), 0);
			Send(users[index].connection, (char*)&answer, sizeof(answer), 0);
		}
		break;
	case P_ConnectWithUser:
		Recv(users[index].connection, friendsName, sizeof(friendsName), 0);
		packettype = P_ChatMessage;
		int checkName = -1;
		if (strcmp(friendsName, users[index].username) == 0)
		{
			strcpy(answer, "You are already friends with yourself :)\n");
			Send(users[index].connection, (char*)&packettype, sizeof(packettype), 0);
			Send(users[index].connection, answer, sizeof(answer), 0);
			return 0;
		}
		else if(strcmp(friendsName, users[users[index].id_companion].username) == 0)
		{
			strcpy(answer, "You are already friends with ");
			strcat(answer, users[users[index].id_companion].username);
			strcat(answer, "!\n");
			Send(users[index].connection, (char*)&packettype, sizeof(packettype), 0);
			Send(users[index].connection, answer, sizeof(answer), 0);
			return 0;
		}
		for (size_t i = 0; i < counterOfUsers; i++)
		{
			if(strstr(friendsName, users[i].username) != 0 && i != index)
			{
				checkName = 1;
				if (users[i].id_companion != -1)
				{
					strcpy(answer, "You are no longer friends with ");
					strcat(answer, users[i].username);
					strcat(answer, "!\n");	
					Send(users[users[i].id_companion].connection, (char*)&packettype, sizeof(packettype), 0);
					Send(users[users[i].id_companion].connection, answer, sizeof(answer), 0);	
					users[users[i].id_companion].id_companion = -1;
				}
				if (users[index].id_companion != -1) 
				{
					strcpy(answer, "You are no longer friends with ");
					strcat(answer, users[index].username);
					strcat(answer, "!\n");	
					Send(users[users[index].id_companion].connection, (char*)&packettype, sizeof(packettype), 0);
					Send(users[users[index].id_companion].connection, answer, sizeof(answer), 0);		
					users[users[index].id_companion].id_companion = -1;	
				}
				
				users[index].id_companion = i;
				users[i].id_companion = index;
				strcpy(answer, "Now you are a friend of ");
				strcat(answer, users[i].username);
				strcat(answer, "!\n");
				Send(users[index].connection, (char*)&packettype, sizeof(packettype), 0);
				Send(users[index].connection, answer, sizeof(answer), 0);
				strcpy(answer, "Now you are a friend of ");
				strcat(answer, users[index].username);
				strcat(answer, "!\n");
				Send(users[i].connection, (char*)&packettype, sizeof(packettype), 0);
				Send(users[i].connection, answer, sizeof(answer), 0);
				break;	
			}
		}
		if (checkName == -1)
		{
			strcpy(answer, "No client found with this name!\n");
			Send(users[index].connection, (char*)&packettype, sizeof(packettype), 0);
			Send(users[index].connection, answer, sizeof(answer), 0);
		}
		break;
	default:
		printf("%s%d%c", "Unrecognized packet: ", packettype, '\n');
	}

	return 0;
}
void* clientHandler(void *p_index)
{
	int index = *((int*)p_index);
	enum Packet packettype;
	while(1)
	{
		if(Recv(users[index].connection, (char*)&packettype, sizeof(enum Packet), 0) == -1) break;
		if(processPacket(index, packettype) == -1) break;
	}
	disconnectClient(index);
}
int main(int argc, char** arg)
{
	strcpy(serverHS.key, "ttMcZBlXR02lf9qBNapzNbAxgOMYvQ5");
	serverHS.protocolVersion = 1.0f;

	Init();
    struct sockaddr_in addr;
    int sizeofaddr = sizeof(addr);
	BindAddr((struct sockaddr_in*)&addr, "127.0.0.1", DEFAULT_PORT, AF_INET);
	int defaultSListen = Socket(AF_INET, SOCK_STREAM, 0);	
	Bind(defaultSListen, (struct sockaddr*)&addr, sizeof(addr));
    Listen(defaultSListen, maxUsers);
	hashSocketsCreatedCheck[DEFAULT_PORT] = 1;
	
	for (int i = 0; i < maxUsers; i++)
	{
		users[i].connection = Accept(defaultSListen, (struct sockaddr*)&addr, &sizeofaddr);
		users[i].id_companion = -1;
		counterOfUsers++;		
		printf("%s%d%s%d%s", "Client connected: #", i, " - port<", DEFAULT_PORT, ">\n");
		int* index = malloc(sizeof(int));
		*index = i;
		ThreadCreate(clientHandler, index);
	}
	
    system("pause");
    return 0;
}
